<?xml version='1.0' encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!-- Target Database set by the command line

<xsl:param name="target.database.document">olinkdb.xml</xsl:param>
-->

<xsl:param name="latex.class.options">twoside,openright</xsl:param>

<!-- Force 'Here' to place table -->
<xsl:param name="table.default.position" select="'[H]'"/>


<!-- Use the Bob Stayton's Tip related to olinking -->
<xsl:param name="current.docid" select="/*/@id"/>

<!-- Use the literal scaling feature -->
<xsl:param name="literal.extensions">scale.by.width</xsl:param>

<!-- We want the TOC links in the titles, and in blue. -->
<xsl:param name="latex.hyperparam">colorlinks,linkcolor=blue,pdfstartview=FitH</xsl:param>

<!-- How deep should recursive sections appear in the TOC? -->
<xsl:param name="toc.section.depth">
  <xsl:choose>
    <xsl:when test="/processing-instruction('asciidoc-toc')">
      <xsl:variable name="depth">
        <xsl:call-template name="pi-attribute">
          <xsl:with-param name="pis" select="/processing-instruction('asciidoc-toc')"/>
          <xsl:with-param name="attribute" select="'maxdepth'"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="$depth != ''"><xsl:value-of select="$depth"/></xsl:when>
        <xsl:otherwise>2</xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>2</xsl:otherwise>
  </xsl:choose>
</xsl:param>

<!-- Depth of the section numbering -->
<xsl:param name="doc.section.depth">
  <xsl:choose>
    <xsl:when test="/processing-instruction('asciidoc-numbered')">
      <xsl:variable name="depth">
        <xsl:call-template name="pi-attribute">
          <xsl:with-param name="pis" select="/processing-instruction('asciidoc-numbered')"/>
          <xsl:with-param name="attribute" select="'maxdepth'"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:choose>
        <xsl:when test="$depth != ''"><xsl:value-of select="$depth"/></xsl:when>
        <xsl:otherwise>6</xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>6</xsl:otherwise>
  </xsl:choose>
</xsl:param>

<!-- DO NOT Put the dblatex logo -->
<xsl:param name="doc.publisher.show">0</xsl:param>

<!-- Show the list of examples too -->
<xsl:param name="doc.lot.show">figure,table,example</xsl:param>

<!-- DocBook like description -->
<xsl:param name="term.breakline">1</xsl:param>

<!-- Manpage titles not numbered -->
<xsl:param name="refentry.numbered">0</xsl:param>

<!-- Use number and title in cross references -->
<xsl:param name="xref.with.number.and.title" select="1"/>


<xsl:template match="parameter">
  <xsl:variable name="name" select="."/>
  <xsl:variable name="target" select="key('id',$name)[1]"/>

  <xsl:choose>
  <xsl:when test="count($target) &gt; 0">
    <!-- Hot link to the parameter refentry -->
    <xsl:call-template name="hyperlink.markup">
      <xsl:with-param name="linkend" select="$name"/>
      <xsl:with-param name="text">
        <xsl:apply-imports/>
      </xsl:with-param>
    </xsl:call-template>
    <!-- Index entry for this parameter -->
    <xsl:text>\index{Parameters!</xsl:text>
    <xsl:value-of select="$name"/>
    <xsl:text>}</xsl:text>
  </xsl:when>
  <xsl:otherwise>
    <!--
    <xsl:message>No reference for parameter: '<xsl:value-of
    select="$name"/>'</xsl:message>
    -->
    <xsl:apply-imports/>
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="sgmltag[@class='xmlpi']">
  <xsl:variable name="name" select="normalize-space(.)"/>
  <xsl:variable name="nameref" select="concat('pi-',translate($name,' ','_'))"/>
  <xsl:variable name="target" select="key('id',$nameref)[1]"/>

  <xsl:choose>
  <xsl:when test="count($target) &gt; 0">
    <!-- Hot link to the parameter refentry -->
    <xsl:call-template name="hyperlink.markup">
      <xsl:with-param name="linkend" select="$nameref"/>
      <xsl:with-param name="text">
        <xsl:apply-imports/>
      </xsl:with-param>
    </xsl:call-template>
    <!-- Index entry for this parameter -->
    <xsl:text>\index{Processing Instructions!</xsl:text>
    <xsl:value-of select="$name"/>
    <xsl:text>}</xsl:text>
  </xsl:when>
  <xsl:otherwise>
    <!--
    <xsl:message>No reference for parameter: '<xsl:value-of
    select="$name"/>'</xsl:message>
    -->
    <xsl:apply-imports/>
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="formalpara[contains(@role, 'define')]/para">
  <xsl:text>\begin{definition}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>\end{definition}</xsl:text>
</xsl:template>

<xsl:template match="simpara[contains(@role, 'attribute')]">
  <xsl:text>\begin{adjustwidth}{100pt}{0pt}</xsl:text>
  <xsl:text>\begin{flushright}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>\end{flushright}</xsl:text>
  <xsl:text>\end{adjustwidth}</xsl:text>
</xsl:template>

<xsl:template match="simpara/phrase[contains(@role, 'external_reference')]">
  <xsl:text>\begin{adjustwidth}{100pt}{0pt}</xsl:text>
  <xsl:text>\begin{flushright}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>\end{flushright}</xsl:text>
  <xsl:text>\end{adjustwidth}</xsl:text>
</xsl:template>

<!-- <xsl:template match="replaceable|optional" mode="latex.programlisting"> -->
<!--    <xsl:param name="co-tagin" select="'&lt;:'"/> -->
<!--    <xsl:param name="rnode" select="/"/> -->
<!--    <xsl:param name="probe" select="0"/> -->

<!--    <xsl:call-template name="verbatim.embed"> 1 -->
<!--      <xsl:with-param name="co-tagin" select="$co-tagin"/> -->
<!--      <xsl:with-param name="rnode" select="$rnode"/> -->
<!--      <xsl:with-param name="probe" select="$probe"/> -->
<!--    </xsl:call-template> -->
<!-- </xsl:template> -->


<xsl:template name="collab.setup">
  <xsl:param name="authors"/>
  <xsl:choose>
  <xsl:when test="$doc.collab.show!='0'">
    <xsl:text>% ------------------
% Collaborators
% ------------------
\renewcommand{\DBKindexation}{
\begin{DBKindtable}
    </xsl:text>
    <xsl:apply-templates select=".//othercredit" mode="collab"/>
    <xsl:text>&#10;\end{DBKindtable}&#10;}&#10;</xsl:text>
  </xsl:when>
  <xsl:otherwise>
    <xsl:text>\renewcommand{\DBKindexation}{}&#10;</xsl:text>
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="othercredit" mode="collab">
  <xsl:text>\DBKinditem{</xsl:text>
  <xsl:value-of select="contrib"/>
  <xsl:text>}{</xsl:text>
  <xsl:apply-templates select="firstname"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="surname"/>
  <xsl:text> \vskip 0.5em </xsl:text>
  <xsl:apply-templates select="affiliation/orgname"/>
  <xsl:text> - </xsl:text>
  <xsl:apply-templates select="affiliation/jobtitle"/>
  <xsl:text>}&#10;</xsl:text>
</xsl:template>

<!-- Colors Handling -->
<xsl:template match="phrase[contains(@role, 'aqua')]">
  <xsl:text>\textcolor{aqua}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'aqua')]">
  <xsl:text>{\color{aqua}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'aqua-background')]">
  <xsl:text>{\sethlcolor{aqua-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'black')]">
  <xsl:text>\textcolor{black}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'black')]">
  <xsl:text>{\color{black}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'black-background')]">
  <xsl:text>\textcolor{white}{{\sethlcolor{black-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'blue')]">
  <xsl:text>\textcolor{blue}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'blue')]">
  <xsl:text>{\color{blue}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'blue-background')]">
  <xsl:text>\textcolor{white}{{\sethlcolor{blue-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'fuchsia')]">
  <xsl:text>\textcolor{fuchsia}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'fuchsia')]">
  <xsl:text>{\color{fuchsia}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'fuchsia-background')]">
  <xsl:text>\textcolor{white}{{\sethlcolor{fuchsia-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'gray')]">
  <xsl:text>\textcolor{gray}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'gray')]">
  <xsl:text>{\color{gray}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'gray-background')]">
  <xsl:text>\textcolor{black}{{\sethlcolor{gray-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'green')]">
  <xsl:text>\textcolor{green}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'green')]">
  <xsl:text>{\color{green}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'green-background')]">
  <xsl:text>\textcolor{black}{{\sethlcolor{green-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'lime')]">
  <xsl:text>\textcolor{lime}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'lime')]">
  <xsl:text>{\color{lime}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'lime-background')]">
  <xsl:text>\textcolor{black}{{\sethlcolor{lime}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'maroon')]">
  <xsl:text>\textcolor{maroon}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'maroon')]">
  <xsl:text>{\color{maroon}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'maroon-background')]">
  <xsl:text>\textcolor{white}{{\sethlcolor{maroon-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'navy')]">
  <xsl:text>\textcolor{navy}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'navy')]">
  <xsl:text>{\color{navy}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'navy-background')]">
  <xsl:text>\textcolor{white}{{\sethlcolor{navy-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'olive')]">
  <xsl:text>\textcolor{olive}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'olive')]">
  <xsl:text>{\color{olive}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'olive-background')]">
  <xsl:text>\textcolor{black}{{\sethlcolor{olive-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'purple')]">
  <xsl:text>\textcolor{purple}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'purple')]">
  <xsl:text>{\color{purple}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'purple-background')]">
  <xsl:text>\textcolor{white}{{\sethlcolor{purple-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'red')]">
  <xsl:text>\textcolor{red}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'red')]">
  <xsl:text>{\color{red}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'red-background')]">
  <xsl:text>\textcolor{white}{{\sethlcolor{red-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'silver')]">
  <xsl:text>\textcolor{silver}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'silver')]">
  <xsl:text>{\color{silver}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'silver-background')]">
  <xsl:text>\textcolor{black}{{\sethlcolor{silver-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'teal')]">
  <xsl:text>\textcolor{teal}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'teal')]">
  <xsl:text>{\color{teal}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'teal-background')]">
  <xsl:text>\textcolor{black}{{\sethlcolor{teal-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'white')]">
  <xsl:text>\textcolor{white}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'white')]">
  <xsl:text>{\color{white}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'white-background')]">
  <xsl:text>\textcolor{black}{{\sethlcolor{white-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<xsl:template match="phrase[contains(@role, 'yellow')]">
  <xsl:text>\textcolor{yellow}{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="para[contains(@role, 'yellow')]">
  <xsl:text>{\color{yellow}</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}</xsl:text>
</xsl:template>
<xsl:template match="phrase[contains(@role, 'yellow-background')]">
  <xsl:text>\textcolor{black}{{\sethlcolor{yellow-bg}\hl{</xsl:text>
  <xsl:call-template name="apply-templates"/>
  <xsl:text>}}}</xsl:text>
</xsl:template>

<!-- Redefine legalnotice to add \legaltitlecolor and
     \legaltitleboxcolor definition from titles's color attribute-->
<xsl:template match="legalnotice">
  <xsl:text>\def\DBKlegaltitle{</xsl:text>
  <xsl:apply-templates select="title"/>
  <xsl:text>}&#10;</xsl:text>
  <xsl:choose>
    <xsl:when test="title/@color">
      <xsl:choose>
        <xsl:when test="contains(title/@color,'++')">
          <xsl:text>\def\legaltitlecolor{</xsl:text>
          <xsl:value-of select="substring-before(title/@color,'++')"/>
          <xsl:text>}&#10;</xsl:text>
          <xsl:text>\def\legaltitleboxcolor{</xsl:text>
          <xsl:value-of select="substring-after(title/@color,'++')"/>
          <xsl:text>}&#10;</xsl:text>
        </xsl:when>
        <xsl:otherwise>
      <xsl:text>\def\legaltitlecolor{</xsl:text>
      <xsl:value-of select="title/@color"/>
      <xsl:text>}&#10;</xsl:text>
          <xsl:text>\def\legaltitleboxcolor{black}&#10;</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <!-- Default value of legal title color -->
      <xsl:text>\def\legaltitlecolor{red}&#10;</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>\begin{DBKlegalnotice}&#10;</xsl:text>
  <xsl:apply-templates select="*[not(self::title)]"/>
  <xsl:text>\end{DBKlegalnotice}&#10;</xsl:text>
</xsl:template>

</xsl:stylesheet>

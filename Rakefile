# frozen_string_literal: true

require 'rake/clean'

describe_cmd_string = 'git describe --abbrev=6 --dirty=\'-Dirty\' ' \
                      '--always 2>/dev/null'

git_describe =  `#{describe_cmd_string}`.chomp

date_cmd_string = 'git log -1 --format=%cd --date=short'

git_date = `#{date_cmd_string}`.chomp

adoc_list = Dir['*.adoc']

asciidoctor = `which asciidoctor`.chomp
defmastership = `which defmastership`.chomp
dblatex = `which dblatex`.chomp

my_tasks = []

my_tasks += %i[html xml] unless asciidoctor == ''
my_tasks += %i[csv] unless defmastership == ''
my_tasks += %i[pdf] unless dblatex == ''

my_tasks.each do |a_task|
  a_list = adoc_list.collect { |f| f.ext(a_task.to_s) }
  task a_task => a_list
  CLEAN.include(a_list)
end

task default: my_tasks

unless asciidoctor == ''
  common_adoc_options = '--trace ' \
                        "-a git-describe=\"#{git_describe}\" " \
                        "-a git-date=\"#{git_date}\" " \
                        '-r asciidoctor-diagram '
  common_adoc_options += '-r asciidoctor/defmastership ' unless defmastership == ''

  rule '.html' => '.adoc' do |task|
    command = "#{asciidoctor} " \
              '-a stylesheet=defmastership-example.css ' \
              "#{common_adoc_options} " \
              "'#{task.source}'"
    sh(command)
  end

  Dir['*-docinfo.xml'].each do |docinfo_file|
    file docinfo_file.gsub('-docinfo.xml', '.xml') =>
         [docinfo_file, docinfo_file.gsub('-docinfo.xml', '.adoc')]
  end

  # build dependencies
  require './asciidoctor_dependencies'
  adoc_list.each do |adoc_file|
    dependencies = Asciidoctor.dependencies_of(adoc_file).map { |file| "./#{file}" }

    dependencies << adoc_file

    file adoc_file.gsub('.adoc', '.html') => dependencies

    docinfo_file = adoc_file.gsub('.adoc', '-docinfo.xml')

    dependencies << docinfo_file if File.exist?(docinfo_file)

    file adoc_file.gsub('.adoc', '.xml') => dependencies
  end

  rule '.xml' => '.adoc' do |task|
    command = "#{asciidoctor} " \
              '--backend=docbook5 ' \
              "#{common_adoc_options} " \
              "'#{task.source}'"
    sh(command)
  end
end

unless dblatex == ''
  latex_style = './dblatex-rendering/defmastership.sty'
  xsl_user = './dblatex-rendering/defmastership-dblatex.xsl'

  rule '.pdf' => ['.xml', latex_style, xsl_user] do |task|
    debug_options = '' # '-d --tmpdir=./tmp'
    command = "#{dblatex} " \
              '-V ' \
              "-o '#{task.name}' " \
              "#{debug_options} " \
              "--texstyle=#{latex_style} " \
              "--xsl-user=#{xsl_user} " \
              "'#{task.source}'"
    sh(command)
  end
end

unless defmastership == ''
  rule '.csv' => '.adoc' do |task|
    command = "#{defmastership} " \
              'export ' \
              '--no-fail ' \
              '--separator=";" ' \
              "'#{task.source}'"
    sh(command)
  end
end

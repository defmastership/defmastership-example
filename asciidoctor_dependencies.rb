require 'asciidoctor'

module Asciidoctor
  def self.dependencies_of(filename)
    load_file(filename, safe: :safe).dependencies
  end

  class Document
    def dependencies
      path_resolver.partition_path_sys.keys.reject { |fname| File.directory?(fname) }
    end
  end

  class PathResolver
    def partition_path_sys
      @_partition_path_sys
    end
  end
end

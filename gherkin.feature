Feature: Including gherkin example in definition
  In order to put gherkin examples in clean documents
  As an explicit system actor
  I want include gherkin example in a Asciidoctor document

  Scenario: Simple include
    Given a asciidoc document
    And the existing gherkin.feature file
    When I include gherkin.feature
    Then the rendering is beautiful
